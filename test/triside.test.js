const { it } = require('mocha');

const Triside = require('../src/triside').Triside;
const expect = require('chai').expect;

describe('Testing the Triangle Functions', function() {
    it('1. The Half Surface of the Triangle', function(done) {
        let f1 = new Triside(4,9,6);
        expect(f1.getHalfSurfaceArea()).to.equal(9.5);
        done();
    });
    it('2. The Surface Area of the Triangle', function(done) {
        let f2 = new Triside(4,9,6);
        expect(f2.getTriSurfaceArea()).to.equal(9.6);
        done();
    });
    
    it('3. The height of the Triangle', function(done) {
        let f3 = new Triside(4,9,6);
        expect(f3.getHeight()).to.equal(4.8);
        done();
    });
    
    it('4. The volume of the Triangle', function(done) {
        let f4 = new Triside(4,9,6);
        expect(f4.getTriVolume()).to.equal(46.1);
        done();
    });
    it('5. Is the triangle isocele ?', function(done)  {
    let f5 = new Triside(4,4,4);
    expect(f5.isTriangleIsoscele()).to.equal(true);
    done();
    });
    
});