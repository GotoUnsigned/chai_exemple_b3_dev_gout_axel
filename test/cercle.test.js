const Cercle = require('../src/cercle').Cercle;
const expect = require('chai').expect;

describe('Testing the Cercle Functions', function() {
    it('1. The base rayon of the Cercle', function(done) {
        let c1 = new Cercle(6);
        expect(c1.getRayon()).to.equal(3);
        done();
    });
    it('2. The Aire of the Cercle', function(done) {
        let c2 = new Cercle(6);
        expect(c2.getCercleAire()).to.equal(9.4);
        done();
    });
});