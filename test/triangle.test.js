const Triangle = require('../src/triangle').Triangle;
const expect = require('chai').expect;

describe('Testing the Triangle Functions', function() {
    it('1. The base length of the Triangle', function(done) {
        let t1 = new Triangle(2,4);
        expect(t1.getBaseLenght()).to.equal(2);
        done();
    });
    it('2. The height length of the Triangle', function(done) {
        let t2 = new Triangle(2,4);
        expect(t2.getHeightLenght()).to.equal(4);
        done();
    });
    
    it('3. The surface area of the Triangle', function(done) {
        let t3 = new Triangle(2,4);
        expect(t3.getTriSurfaceArea()).to.equal(4);
        done();
    });
    
    it('4. The volume of the Triangle', function(done) {
        let t4 = new Triangle(2,4);
        expect(t4.getTriVolume()).to.equal(16);
        done();
    });
    
});