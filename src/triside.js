class Triside{
    constructor(side1,side2,side3){
    this.side1=side1;
    this.side2=side2;
    this.side3=side3;
}

getHalfSurfaceArea(){
    return((this.side1 + this.side2 + this.side3)/2);
}
getTriSurfaceArea(){

    return Number(Math.sqrt(this.getHalfSurfaceArea()*(this.getHalfSurfaceArea()-this.side1)*(this.getHalfSurfaceArea()-this.side2)*(this.getHalfSurfaceArea()-this.side3)).toFixed(1));
}
getHeight(){
    return((2*this.getTriSurfaceArea()) / this.side1);
}
getTriVolume(){
    return Number((this.getTriSurfaceArea() * this.getHeight()).toFixed(1));

}
isTriangleIsoscele(){
    let isTriangleIsoscele = false;
    if(this.side1 === this.side2 | this.side1 === this.side3 | this.side2 === this.side3) isTriangleIsoscele = true
    return isTriangleIsoscele;
}

}
module.exports = {
    Triside:Triside
}