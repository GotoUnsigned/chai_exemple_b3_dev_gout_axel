class Cercle{
    constructor(diameter){
    this.diameter=diameter;
}

getRayon(){
    return(this.diameter / 2);
}

getCercleAire(){
    return Number((this.getRayon()*Math.PI).toFixed(1));
}
}

module.exports = {
    Cercle:Cercle
}