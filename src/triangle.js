class Triangle{
    constructor(baseLength,heightLength){
        this.baseLength = baseLength;
        this.heightLength = heightLength;
    }

    getBaseLenght(){
        return this.baseLength;
    }

    getHeightLenght(){
        return this.heightLength;
    }

    getTriSurfaceArea(){
        return (this.baseLength * this.heightLength) / 2;
        
    }

    getTriVolume(){
        return (((this.baseLength * this.heightLength) / 2)* this.heightLength);
    }
}
module.exports = {
    Triangle:Triangle
}